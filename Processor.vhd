library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.SYKO.all;

entity Processor is
	generic
	(
		DataLength : integer := 5;
		InstructionLengthInBytes : integer := 3;
		Delay : time := 1 ns
	);
	port
	(
		clk		: in STD_LOGIC;	
		rst		: in std_logic;
		


	out_dataIn : out std_logic_vector(DataLength - 1 downto 0 );
				out_test		: out std_logic_vector(23 downto 0 );
	out_MR : out  std_logic;
	out_MW : out  std_logic;
	out_RE_ToMemory : out  std_logic;
	out_RE_ToBus : out  std_logic;

	out_WE_FromMemory : out  std_logic;
	out_WE_FromBus : out  std_logic;

	out_DECODE : out  std_logic;
	out_CIR : out  std_logic;
	out_INCR : out  std_logic;
	out_OE_RegisterIR : out std_logic;

	out_IE_RegisterIMR: out  std_logic;
	out_OE1_RegisterIMR: out  std_logic;
	out_OE2_RegisterIMR: out  std_logic;
	out_OE3_RegisterIMR: out  std_logic;

	out_IE_RegisterIMRA: out  std_logic;
	out_OE1_RegisterIMRA: out  std_logic;
	out_OE2_RegisterIMRA: out  std_logic;

	out_IE_RegisterA: out  std_logic;
	out_IE2_RegisterA : out std_logic;
	out_OE1_RegisterA: out  std_logic;
	out_OE2_RegisterA: out  std_logic;
	out_OE3_RegisterA: out  std_logic;
	out_OE4_RegisterA: out  std_logic;

	out_IE_RegisterB: out  std_logic;
	out_IE2_RegisterB : out  std_logic;
	out_OE1_RegisterB: out  std_logic;
	out_OE2_RegisterB: out  std_logic;
	out_OE3_RegisterB: out  std_logic;
	out_OE4_RegisterB: out  std_logic;

	out_LAE : out  std_logic;
	out_CALU : out  std_logic; -- gdy 1 to CMP

	out_Cag_FromPC : out  std_logic;
	out_Cag_FromIMRA : out  std_logic;
	out_Cag_FromA : out  std_logic;
	out_Cag_FromB : out  std_logic;

	out_ALU_ToFLAGS: out std_logic_vector (2 downto 0); --sygna� z alu do rejestru flags

	out_MUX_IsFromBus : out  std_logic;
	out_MUX_Out : out std_logic_vector(7 downto 0);

	out_IE_RegisterFLAGS: out std_logic;
	out_OE_RegisterFLAGS: out  std_logic;

	out_fullInstruction : out std_logic_vector( 8*InstructionLengthInBytes - 1 downto 0);
	out_DB : out std_logic_vector( 7 downto 0);
	out_ADR : out std_logic_vector (7 downto 0);
	out_address : out std_logic_vector(7 downto 0);
	out_memoryValue : out std_logic_vector(7 downto 0);

	out_X_Bus : out std_logic_vector(7 downto 0);
	out_Y_Bus : out std_logic_vector(7 downto 0);
	out_Z_Bus : out std_logic_vector(7 downto 0);

	out_AG_FromPC : out std_logic_vector(7 downto 0);
	out_AG_FromIMRA : out std_logic_vector(7 downto 0);
	out_AG_FromA : out std_logic_vector(7 downto 0);
	out_AG_FromB : out std_logic_vector(7 downto 0);
	out_AG_FromIMR : out std_logic_vector(7 downto 0);
	out_memory : out ram
	);
end Processor;

architecture ProcessorArchitecture of Processor is

component CU is
	generic
	(
		DataLength : integer := 5;
		Delay : time := Delay
	);
	port(
		clk		: IN STD_LOGIC;	
		rst		: in std_logic;
		
		dataIn : in std_logic_vector(DataLength - 1 downto 0 );
		
		MR	: out std_logic;
		MW : out std_logic;
		RE_ToMemory : out std_logic;
		RE_ToBus : out std_logic;
		
		WE_FromMemory : out std_logic;
		WE_FromBus : out std_logic;
		
		DECODE : out std_logic;
		CIR : out std_logic;
		INCR : out std_logic;
		OE_RegisterIR : out std_logic;
		
		IE_RegisterIMR: out std_logic;
		OE1_RegisterIMR: out std_logic;
		OE2_RegisterIMR: out std_logic;
		OE3_RegisterIMR: out std_logic;
		
		IE_RegisterIMRA: out std_logic;
		OE1_RegisterIMRA: out std_logic;
		OE2_RegisterIMRA: out std_logic;
				
		IE_RegisterA: out std_logic;
		IE2_RegisterA : out std_logic;
		OE1_RegisterA: out std_logic;
		OE2_RegisterA: out std_logic;
		OE3_RegisterA: out std_logic;
		OE4_RegisterA: out std_logic;
		
		IE_RegisterB: out std_logic;
		IE2_RegisterB : out std_logic;
		OE1_RegisterB: out std_logic;
		OE2_RegisterB: out std_logic;
		OE3_RegisterB: out std_logic;
		OE4_RegisterB: out std_logic;
		
		LAE : out std_logic;
		CALU : out std_logic; -- gdy 1 to CMP
		
		Cag_FromPC : out std_logic;
		Cag_FromIMRA : out std_logic;
		Cag_FromA : out std_logic;
		Cag_FromB : out std_logic;
		
		MUX_IsFromBus : out std_logic;
		
		OE_RegisterFLAGS: out std_logic;
		IE_RegisterFLAGS: out std_logic
		);
end component CU;

component ID is
	generic
	(
		InstructionLength : integer := 24;
		Delay : time := 1 ns
	);
	port(
		clk		: IN STD_LOGIC;	
		rst		: IN STD_LOGIC;
		
		dataIn : in std_logic_vector(InstructionLength - 1 downto 0 );
		dataOut: out std_logic_vector(4 downto 0 );
		DECODE : in std_logic
		);
end component ID;

component IR is
	generic
	(
		DataLength : integer := 8;
		InstructionLengthInBytes : integer := 3;
		Delay : time := Delay
	);
	port(
		clk		: IN STD_LOGIC;
		rst		: IN std_logic;
		test		: out std_logic_vector(DataLength * InstructionLengthInBytes  - 1 downto 0 );
		dataIn 	: in std_logic_vector(DataLength - 1 downto 0 );
		dataOut	: out std_logic_vector(DataLength * InstructionLengthInBytes  - 1 downto 0 );
		IE 		: in std_logic;
		OE 		: in std_logic
		);
end component IR;

component MAR is
	generic
	(
		AddressLength : integer := 8;
		Delay : time := Delay
	);
	port(
		clk : IN std_logic;
		rst :		IN std_logic;

		dataIn : in std_logic_vector(AddressLength - 1 downto 0 );
		dataOut: out std_logic_vector(AddressLength - 1 downto 0 );
		LAE : in std_logic
		);
end component MAR;

component MBR is
	generic
	(
		DataLength : integer := 8;
		Delay : time := Delay
	);
	port(
		clk : in std_logic;
		rst : in std_logic;
		memoryData : inout std_logic_vector(DataLength - 1 downto 0 );
		busData: inout std_logic_vector(DataLength - 1 downto 0 );
		RE_ToMemory : in std_logic;
		RE_ToBus : in std_logic;
		WE_FromMemory : in std_logic;
		WE_FromBus : in std_logic
		);
end component MBR;

component Memory is
generic
(
	Delay : time := Delay
);
port(
  rst : in std_logic;
  address : in std_logic_vector  (7 downto 0);
  data : inout std_logic_vector  (7 downto 0);
  memory : out ram;
  MR : in std_logic;
  MW : in std_logic
  );
end component Memory;

component A is

generic(delay : time := 1 ns);

port( 
	clk : in std_logic;
 reset : in std_logic;
  data_mux : in std_logic_vector  (7 downto 0); --wejście z multipleksera
  data_x : out std_logic_vector  (7 downto 0); --wyjście na szynę lokalną X
  data_y : out std_logic_vector  (7 downto 0); --wyjście na szynę lokalną Y
  data_db  : inout std_logic_vector  (7 downto 0); --wyjście na szynę danych
  data_ag  : out std_logic_vector  (7 downto 0); -- wyjścię do generatora adresów
  IE : in std_logic; --zezwolenie na odczyt z szyny db
  IE2 : in std_logic; --zezwolenie na odczyt z multipleksera
  OE1 : in std_logic; --zezwolenie na zapis na szynę danych
  OE2 : in std_logic; --zezwolenie na zapis do generatora adresów
  OE3 : in std_logic; --zezwolenie na zapis na szynę X
  OE4 : in std_logic --zezwolenie na zapis na szynę Y
  );
end component A;

component PC is

generic(delay : time := 1 ns);

port(
clk : in std_logic;
INCR : in std_logic;
reset : in std_logic;
outAG : out std_logic_vector (7 downto 0):="00000000"
);
end component PC;

component IMRA is

generic(delay : time := 1 ns);

port( 
  clk : in std_logic;
  reset : in std_logic;
  data_db : inout std_logic_vector  (7 downto 0);
  data_ag : out std_logic_vector  (7 downto 0);
  IE : in std_logic; --db
  OE_1 : in std_logic; -- db
  OE_2 : in std_logic -- ag
  );
end component IMRA;	

component IMR is

generic(delay : time := 1 ns);

port( 
  clk : in std_logic;
  reset : in std_logic;
  data_db : inout std_logic_vector  (7 downto 0);
  data_ag : out std_logic_vector  (7 downto 0);
  data_x  : out std_logic_vector  (7 downto 0); 
  IE : in std_logic; --db
  OE_1 : in std_logic; -- db
  OE_2 : in std_logic; -- ag
  OE_3 : in std_logic -- x
  );
end component IMR;	

component FLAGS is

generic(delay : time := 1 ns);

port( reset : in std_logic;
  data_out : out std_logic_vector  (7 downto 0);
  inALU : in std_logic_vector  (2 downto 0); --bit2-ZF, bit1-SF, bit0-OF 
  IE : in std_logic;
  OE : in std_logic
  );
end component FLAGS;

component ALU is

generic(delay : time := 1 ns);

port(
  data1 : in std_logic_vector  (7 downto 0); --argument �r�d�owy
  data2 : in std_logic_vector  (7 downto 0); -- argument docelowy
  flags : out std_logic_vector  (2 downto 0);
  data_out : out std_logic_vector (7 downto 0);
  CALU : in std_logic
  );
end component ALU;
		
		
component MUX is

generic(delay : time := 1 ns);
port( 
  data_db : in std_logic_vector  (7 downto 0);
  data_out : out std_logic_vector  (7 downto 0);
  data_z  : in std_logic_vector  (7 downto 0); 
  from_bus : in std_logic --db
  );
end component MUX;	
		
component AG is

generic(delay : time := 1 ns);

port( IE_IMRA: in std_logic;
      IE_A: in std_logic;
      IE_B: in std_logic;
      IE_PC: in std_logic;
      data_imra : in STD_LOGIC_VECTOR (7 downto 0);
      data_a : in STD_LOGIC_VECTOR (7 downto 0);
      data_b : in STD_LOGIC_VECTOR (7 downto 0);
      data_pc : in STD_LOGIC_VECTOR (7 downto 0);
      address : out STD_LOGIC_VECTOR (7 downto 0));
end component AG;
		
signal		dataIn : std_logic_vector(DataLength - 1 downto 0 );
		
signal		MR	:  std_logic;
signal		MW :  std_logic;
signal		RE_ToMemory :  std_logic;
signal		RE_ToBus :  std_logic;
		
signal		WE_FromMemory :  std_logic;
signal		WE_FromBus :  std_logic;
		
signal		DECODE :  std_logic;
signal		CIR :  std_logic;
signal		INCR :  std_logic;
signal 		OE_RegisterIR : std_logic;
		
signal		IE_RegisterIMR:  std_logic;
signal		OE1_RegisterIMR:  std_logic;
signal		OE2_RegisterIMR:  std_logic;
signal		OE3_RegisterIMR:  std_logic;
		
signal		IE_RegisterIMRA:  std_logic;
signal		OE1_RegisterIMRA:  std_logic;
signal		OE2_RegisterIMRA:  std_logic;
				
signal		IE_RegisterA:  std_logic;
signal		IE2_RegisterA : std_logic;
signal		OE1_RegisterA:  std_logic;
signal		OE2_RegisterA:  std_logic;
signal		OE3_RegisterA:  std_logic;
signal		OE4_RegisterA:  std_logic;
		
signal		IE_RegisterB:  std_logic;
signal		IE2_RegisterB :  std_logic;
signal		OE1_RegisterB:  std_logic;
signal		OE2_RegisterB:  std_logic;
signal		OE3_RegisterB:  std_logic;
signal		OE4_RegisterB:  std_logic;
		
signal		LAE :  std_logic;
signal		CALU :  std_logic; -- gdy 1 to CMP
		
signal		Cag_FromPC :  std_logic;
signal		Cag_FromIMRA :  std_logic;
signal		Cag_FromA :  std_logic;
signal		Cag_FromB :  std_logic;

signal		ALU_ToFLAGS: std_logic_vector (2 downto 0); --sygna� z alu do rejestru flags
		
signal		MUX_IsFromBus :  std_logic;
signal		MUX_Out : std_logic_vector(7 downto 0);

signal 		IE_RegisterFLAGS: std_logic;
signal		OE_RegisterFLAGS:  std_logic;

signal 		fullInstruction : std_logic_vector( 	8*	InstructionLengthInBytes - 1 downto 0);
signal 		DB : std_logic_vector( 7 downto 0);
signal		ADR : std_logic_vector (7 downto 0);
signal		address : std_logic_vector(7 downto 0);
signal		memoryValue : std_logic_vector(7 downto 0);

signal 		X_Bus : std_logic_vector(7 downto 0);
signal		Y_Bus : std_logic_vector(7 downto 0);
signal		Z_Bus : std_logic_vector(7 downto 0);

signal		AG_FromPC : std_logic_vector(7 downto 0);
signal		AG_FromIMRA : std_logic_vector(7 downto 0);
signal		AG_FromA : std_logic_vector(7 downto 0);
signal 		AG_FromB : std_logic_vector(7 downto 0);
signal		AG_FromIMR :std_logic_vector(7 downto 0);

constant clk_period : time := 1 ns;
begin
out_dataIn <= dataIn;
   
out_MR <= MR;
out_MW <= MW;
out_RE_ToMemory <= RE_ToMemory;
out_RE_ToBus <= RE_ToBus;
   
out_WE_FromMemory <= WE_FromMemory;
out_WE_FromBus <= WE_FromBus;
   
out_DECODE <= DECODE;
out_CIR <= CIR;
out_INCR <= INCR;
out_OE_RegisterIR <= OE_RegisterIR;
   
out_IE_RegisterIMR <= IE_RegisterIMR;
out_OE1_RegisterIMR <= OE1_RegisterIMR;
out_OE2_RegisterIMR <= OE2_RegisterIMR;
out_OE3_RegisterIMR <= OE3_RegisterIMR;
   
out_IE_RegisterIMRA <= IE_RegisterIMRA;
out_OE1_RegisterIMRA <= OE1_RegisterIMRA;
out_OE2_RegisterIMRA <= OE2_RegisterIMRA;
   
out_IE_RegisterA <= IE_RegisterA;
out_IE2_RegisterA <= IE2_RegisterA;
out_OE1_RegisterA <= OE1_RegisterA;
out_OE2_RegisterA <= OE2_RegisterA;
out_OE3_RegisterA <= OE3_RegisterA;
out_OE4_RegisterA <= OE4_RegisterA;
   
out_IE_RegisterB <= IE_RegisterB;
out_IE2_RegisterB <= IE2_RegisterB;
out_OE1_RegisterB <= OE1_RegisterB;
out_OE2_RegisterB <= OE2_RegisterB;
out_OE3_RegisterB <= OE3_RegisterB;
out_OE4_RegisterB <= OE4_RegisterB;
   
out_LAE <= LAE;
out_CALU <= CALU;
   
out_Cag_FromPC <= Cag_FromPC;
out_Cag_FromIMRA <= Cag_FromIMRA;
out_Cag_FromA <= Cag_FromA;
out_Cag_FromB <= Cag_FromB;
   
out_ALU_ToFLAGS <= ALU_ToFLAGS;
 
out_MUX_IsFromBus <= MUX_IsFromBus;
out_MUX_Out <= MUX_Out;
   
out_IE_RegisterFLAGS <= IE_RegisterFLAGS;
out_OE_RegisterFLAGS <= OE_RegisterFLAGS;
   
out_fullInstruction <= fullInstruction;
out_DB <= DB;
out_ADR <= ADR;
out_address <= address;
out_memoryValue <= memoryValue;
   
out_X_Bus <= X_Bus;
out_Y_Bus <= Y_Bus;
out_Z_Bus <= Z_Bus;
   
out_AG_FromPC <= AG_FromPC;
out_AG_FromIMRA <= AG_FromIMRA;
out_AG_FromA <= AG_FromA;
out_AG_FromB <= AG_FromB;
out_AG_FromIMR <= AG_FromIMR;

cuunit : CU port map ( clk, rst, dataIn, MR, MW, RE_ToMemory, RE_ToBus, WE_FromMemory, WE_FromBus, DECODE, CIR, INCR,OE_RegisterIR, IE_RegisterIMR, OE1_RegisterIMR, OE2_RegisterIMR, OE3_RegisterIMR, IE_RegisterIMRA, OE1_RegisterIMRA, OE2_RegisterIMRA, IE_RegisterA , IE2_RegisterA, OE1_RegisterA, OE2_RegisterA, OE3_RegisterA, OE4_RegisterA, IE_RegisterB, IE2_RegisterB, OE1_RegisterB, OE2_RegisterB, OE3_RegisterB, OE4_RegisterB, LAE, CALU, Cag_FromPC, Cag_FromIMRA, Cag_FromA, Cag_FromB, MUX_IsFromBus, OE_RegisterFLAGS, IE_RegisterFLAGS);
idunit : ID port map ( clk, rst, fullInstruction, dataIn, DECODE);
irunit : IR port map ( clk, rst, out_test,DB, fullInstruction, CIR, OE_RegisterIR);
marunit: MAR port map (	clk, rst,ADR,	address,	LAE);
mbrunit: MBR port map ( clk, rst, memoryValue, DB, RE_ToMemory, RE_ToBus, WE_FromMemory, WE_FromBus);
memunit: Memory port map ( rst, address, memoryValue, out_memory, MR, MW );
pcunit: PC port map (clk, INCR, rst, AG_FromPC);
imraunit: IMRA port map (clk, rst, DB, AG_FromIMRA, IE_RegisterIMRA, OE1_RegisterIMRA, OE2_RegisterIMRA);
imrunit: IMR port map (clk,rst, DB, AG_FromIMR, X_Bus, IE_RegisterIMR, OE1_RegisterIMR, OE2_RegisterIMR, OE3_RegisterIMR);
flagsunit: FLAGS port map (rst, DB, ALU_ToFLAGS, IE_RegisterFLAGS, OE_RegisterFLAGS);
aluunit: ALU port map (X_Bus, Y_Bus, ALU_ToFLAGS, Z_Bus, CALU);
agunit: AG port map (Cag_FromIMRA, Cag_FromA, Cag_FromB, Cag_FromPC, AG_FromIMRA, AG_FromA, AG_FromB, AG_FromPC, ADR);
aunit: A port map (clk,rst, MUX_Out, X_Bus, Y_Bus, DB, AG_FromA,IE_RegisterA, IE2_RegisterA, OE1_RegisterA, OE2_RegisterA, OE3_RegisterA, OE4_RegisterA);
bunit: A port map (clk,rst, MUX_Out, X_Bus, Y_Bus, DB, AG_FromB,IE_RegisterB, IE2_RegisterB, OE1_RegisterB, OE2_RegisterB, OE3_RegisterB, OE4_RegisterB);
muxunit: MUX port map (DB, MUX_Out, Z_Bus, MUX_IsFromBus);

end ProcessorArchitecture;