library IEEE; 
use IEEE.STD_LOGIC_1164.all;

entity ALU is

generic(delay : time := 1 ns);

port(
  data1 : in std_logic_vector  (7 downto 0); --argument �r�d�owy- wej�cie z szyny X
  data2 : in std_logic_vector  (7 downto 0); -- argument docelowy- wej�cie z szyny Y
  flags : out std_logic_vector  (2 downto 0); --wyj�cie na rejestr znacznik�w flags(2)=ZF, flags(1)=SF, flags(0)=OF
  data_out : out std_logic_vector (7 downto 0);--wyjscie na szyn� Z, w naszym przypadku nieu�ywane
  CALU : in std_logic --sygna� steruj�cy ALU, gdy CALU=1 to wykonywana jest operacja CMP
  );
end ALU;


architecture behav of ALU is

begin
  process(data1, data2, CALU)
	variable vartemp2 : std_logic_vector(7 downto 0);
	variable negdata: std_logic_vector(7 downto 0);
	variable carry: std_logic;
	variable i: integer;
	variable one: std_logic_vector(7 downto 0);
	variable diff: std_logic_vector(7 downto 0);
begin
  
  case CALU is
  when '1' =>
	vartemp2 := not data2;
	carry :='0';
	one := "00000001";
  	L1: for i in 0 to 7 loop
    	negdata(i) := (carry xor vartemp2(i)) xor one(i);
    	carry := (vartemp2(i) and one(i)) or ((vartemp2(i) xor one(i)) and carry);
   	end loop;
	data_out <=negdata;
	
	carry := '0';
	L2: for i in 0 to 7 loop
    	diff(i) := (carry xor data1(i)) xor negdata(i);
    	carry := (negdata(i) and data1(i)) or ((negdata(i) xor data1(i)) and carry);
   	end loop;
	
	data_out<="ZZZZZZZZ";

if diff="00000000" then
	flags(2) <='1' after delay;
else
	flags(2) <= '0' after delay;
end if;


if diff(7)='1' then
	flags(1) <= '1' after delay;
else
	flags(1) <='0' after delay;
end if;

  if (data1(7)=negdata(7) and data1(7)/=diff(7)) then
    flags(0) <= '1' after delay;
  else
  flags(0) <= '0' after delay;
  end if;

  when others =>
  data_out <= "ZZZZZZZZ" after delay;
  flags <= "ZZZ" after delay;
  end case;
  end process;
end behav;


