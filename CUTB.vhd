library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CUTB is
	generic
	(
		DataLength : integer := 5
	);
end CUTB;

architecture CUTBArchitecture of CUTB is

component CU is
	generic
	(
		DataLength : integer := 5
	);
	port(
		clk		: IN STD_LOGIC;	
		rst		: in std_logic;
		
		dataIn : in std_logic_vector(DataLength - 1 downto 0 );
		
		MR	: out std_logic;
		MW : out std_logic;
		RE_ToMemory : out std_logic;
		RE_ToBus : out std_logic;
		
		WE_FromMemory : out std_logic;
		WE_FromBus : out std_logic;
		
		DECODE : out std_logic;
		CIR : out std_logic;
		INCR : out std_logic;
		
		IE_RegisterIMR: out std_logic;
		OE1_RegisterIMR: out std_logic;
		OE2_RegisterIMR: out std_logic;
		OE3_RegisterIMR: out std_logic;
		
		IE_RegisterIMRA: out std_logic;
		OE1_RegisterIMRA: out std_logic;
		OE2_RegisterIMRA: out std_logic;
				
		IE_RegisterA: out std_logic;
		IE2_RegisterA : out std_logic;
		OE1_RegisterA: out std_logic;
		OE2_RegisterA: out std_logic;
		OE3_RegisterA: out std_logic;
		OE4_RegisterA: out std_logic;
		
		IE_RegisterB: out std_logic;
		IE2_RegisterB : out std_logic;
		OE1_RegisterB: out std_logic;
		OE2_RegisterB: out std_logic;
		OE3_RegisterB: out std_logic;
		OE4_RegisterB: out std_logic;
		
		LAE : out std_logic;
		CALU : out std_logic; -- gdy 1 to CMP
		
		Cag_FromPC : out std_logic;
		Cag_FromIMRA : out std_logic;
		Cag_FromA : out std_logic;
		Cag_FromB : out std_logic;
		
		MUX_IsFromBus : out std_logic;
		
		OE_RegisterFLAGS: out std_logic
		);
end component CU;

signal		clk		: STD_LOGIC;	
signal		rst		: std_logic;
		
signal		dataIn : std_logic_vector(DataLength - 1 downto 0 );
		
signal		MR	:  std_logic;
signal		MW :  std_logic;
signal		RE_ToMemory :  std_logic;
signal		RE_ToBus :  std_logic;
		
signal		WE_FromMemory :  std_logic;
signal		WE_FromBus :  std_logic;
		
signal		DECODE :  std_logic;
signal		CIR :  std_logic;
signal		INCR :  std_logic;
		
signal		IE_RegisterIMR:  std_logic;
signal		OE1_RegisterIMR:  std_logic;
signal		OE2_RegisterIMR:  std_logic;
signal		OE3_RegisterIMR:  std_logic;
		
signal		IE_RegisterIMRA:  std_logic;
signal		OE1_RegisterIMRA:  std_logic;
signal		OE2_RegisterIMRA:  std_logic;
				
signal		IE_RegisterA:  std_logic;
signal		IE2_RegisterA : std_logic;
signal		OE1_RegisterA:  std_logic;
signal		OE2_RegisterA:  std_logic;
signal		OE3_RegisterA:  std_logic;
signal		OE4_RegisterA:  std_logic;
		
signal		IE_RegisterB:  std_logic;
signal		IE2_RegisterB :  std_logic;
signal		OE1_RegisterB:  std_logic;
signal		OE2_RegisterB:  std_logic;
signal		OE3_RegisterB:  std_logic;
signal		OE4_RegisterB:  std_logic;
		
signal		LAE :  std_logic;
signal		CALU :  std_logic; -- gdy 1 to CMP
		
signal		Cag_FromPC :  std_logic;
signal		Cag_FromIMRA :  std_logic;
signal		Cag_FromA :  std_logic;
signal		Cag_FromB :  std_logic;
		
signal		MUX_IsFromBus :  std_logic;
	
signal		OE_RegisterFLAGS:  std_logic;

constant clk_period : time := 1 ns;
begin

uut : CU port map ( clk, rst, dataIn, MR, MW, RE_ToMemory, RE_ToBus, WE_FromMemory, WE_FromBus, DECODE, CIR, INCR, IE_RegisterIMR, OE1_RegisterIMR, OE2_RegisterIMR, OE3_RegisterIMR, IE_RegisterIMRA, OE1_RegisterIMRA, OE2_RegisterIMRA, IE_RegisterA, IE2_RegisterA, OE1_RegisterA, OE2_RegisterA, OE3_RegisterA, OE4_RegisterA, IE_RegisterB, IE2_RegisterB, OE1_RegisterB, OE2_RegisterB, OE3_RegisterB, OE4_RegisterB, LAE, CALU, Cag_FromPC, Cag_FromIMRA, Cag_FromA, Cag_FromB, MUX_IsFromBus, OE_RegisterFLAGS );

clk_process :process
   begin
        clk <= '1';
        wait for clk_period/2;   
        clk <= '0';
        wait for clk_period/2;   
   end process;

  stim_proc: process
   begin       
		rst <= '1';
		wait for 1 ns;
		
		rst <= '0';
		dataIn <= "00001";
        wait for 1 ns;
		dataIn <= "00000";	
	  wait for 10 ns;
	  dataIn <= "00010";	

        wait;
  end process;
	

end CUTBArchitecture;