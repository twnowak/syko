library IEEE; 
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_unsigned.all; 
use work.SYKO.all;

entity Memory is
generic
(
	Delay : time := 1 ns
);
port(
  rst : in std_logic;
  address : in std_logic_vector  (7 downto 0);
  data : inout std_logic_vector  (7 downto 0);
  memory : out ram;
  MR : in std_logic;
  MW : in std_logic
  );
end Memory;

architecture MemoryArchitecture of Memory is

--type ram is array (255 downto 0) of std_logic_vector (7 downto 0);
signal reg: ram;

	
begin

memory <= reg;
 
process (MR, MW, address)
begin
	if(rst = '1') then
		reg(0)<= "00101110";	-- CMP(A, 0x87)   
		reg(1)<= "10000111";	-- 0x87 (-121)
		reg(2)<= "00000010";	-- offset, nieuzywany
		-- zdekodowany: 00010 (dataIn)

		--STOR(B/P, N/R)
		reg(3)<= "10100011";	-- STOR([B], 0x89) = STOR(B, N)
		reg(4)<= "10001001";	-- 0x89 (-119)
		reg(5)<= "00001000"; -- offset, nieuzywany
		-- zdekodowany: 10011	 (dataIn)
		
		reg(6)<= "01001110";	-- LOAD(A, [B])   
		reg(7)<= "11001011";	-- arg natymichastowy, nieuzywany
		reg(8)<= "00110100";	-- offset, nieuzywany
		-- zdekodowany: 01010 (dataIn)
		
		reg(9)<= "10110010";	 -- STOR([A], B) = STOR(B, R)
		reg(10)<= "10001001"; -- arg natymichastowy, nieuzywany
		reg(11)<= "00010000"; -- offset, nieuzywany
		-- zdekodowany: 11010 (dataIn)
		
		reg(12)<= "10100111"; -- STOR([0x87], 0x89) = STOR(P, N)
		reg(13)<= "10001001"; -- 0x89 (-119)
		reg(14)<= "10000111"; -- 0x87
		-- zdekodowany: 10111 (dataIn)

		reg(15)<= "10110110"; -- STOR([0x88], B) = STOR(P, R)
		reg(16)<= "10001001"; -- arg natymichastowy, nieuzywany
		reg(17)<= "10001000"; -- 0x88 
		-- zdekodowany: 11110 (dataIn)
		
		reg(18)<= "01011110";	-- LOAD(A, [21])   
		reg(19)<= "10000111";	-- nieuzywamy
		reg(20)<= "00010101";	--  przemieszczenie 21
		-- zdekodowany: 01110 (dataIn)

		reg(21)<= "00111110";	-- CMP(A, B)   
		reg(22)<= "00000011";	-- 0x03 (3)
		reg(23)<= "00000010";	-- offset, nieuzywany
		-- zdekodowany: 00110 (dataIn)
		
	elsif MW = '1' 
	then
		reg(conv_integer(address)) <= data  after delay;
	elsif MR = '1' 
	then
		data <= reg(conv_integer(address))  after delay;    
	else
		data <= (others => 'Z')  after delay;               
	end if;

end process;
end MemoryArchitecture;
