library IEEE;
use IEEE.STD_LOGIC_1164.all;
entity AG is

generic
(
delay : time := 1 ns
);

port( IE_IMRA: in std_logic;--zezwolenie na odczyt z rejestru IMRA
      IE_A: in std_logic;--zezwolenie na odczyt z rejestru A
      IE_B: in std_logic;--zezwolenie na odczyt z rejestru B
      IE_PC: in std_logic;--zezwolenie na odczyt z rejestru PC
      data_imra : in STD_LOGIC_VECTOR (7 downto 0);--wej�cie z rejestru IMRA
      data_a : in STD_LOGIC_VECTOR (7 downto 0);--wej�cie z rejestru A
      data_b : in STD_LOGIC_VECTOR (7 downto 0);--wej�cie z rejestru B
      data_pc : in STD_LOGIC_VECTOR (7 downto 0);--wej�cie z rejestru PC
      address : out STD_LOGIC_VECTOR (7 downto 0));--wyj�cie na szyn� adresow�
end AG;


architecture behav of AG is
begin

process (IE_IMRA, IE_A, IE_B, IE_PC, data_imra, data_a, data_b, data_pc)
begin
if IE_IMRA = '1' then
	address <= data_imra after delay;

elsif IE_A = '1' then
	address <= data_a after delay;
elsif IE_B = '1' then
	address <= data_b after delay;
	
elsif IE_PC = '1' then
	address <= data_pc after delay;
	
	
else
	address <= "ZZZZZZZZ" after delay;
	
end if;	
end process;
end behav;

