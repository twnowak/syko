library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ID is
	generic
	(
		InstructionLength : integer := 24;
		Delay : time := 1 ns
	);
	port(
		clk		: IN STD_LOGIC;	
		rst		: IN STD_LOGIC;
		
		dataIn : in std_logic_vector(InstructionLength - 1 downto 0 );
		dataOut: out std_logic_vector(4 downto 0 );
		DECODE : in std_logic
		);
end ID;

architecture IDArchitecture of ID is

signal tmp : std_logic_vector( 1 downto 0 );

signal reg : std_logic_vector(InstructionLength - 1 downto 0);
begin

tmp <= dataIn(4) & dataIn(2);



extractProcess: process (clk, dataIn, DECODE)
begin
if rst = '1' then
	dataOut <= (others => 'Z') after delay;
elsif rising_edge(clk)
then
	reg <= dataIn after delay;
elsif falling_edge(clk)
then
	if ( DECODE = '1' )
	then
		-- instruction type
		if reg(7 downto 6) = "00" then
			-- CMP(R, N/R)
			if reg(4) = '0' then --N
				dataOut <= "000" & reg(1 downto 0) after delay;
			elsif reg(4) = '1' then --R
				dataOut <= "001" & reg(1 downto 0) after delay;
			else
				dataOut <= (others => 'Z') after delay;
			end if;
		elsif reg(7 downto 6) = "01" then
			-- LOAD(R, B/P)
				if reg(4) = '0' then
					dataOut <= "010" & reg(1 downto 0) after delay;
				elsif reg(4) = '1' then
					dataOut <= "011" & reg(1 downto 0) after delay;
				else
					dataOut <= (others => 'Z');
				end if;
		elsif reg(7 downto 6) = "10" then
			--STOR(B/P, N/R)
				if tmp = "00" then --STOR(B, N)
					dataOut <= "100" & reg(1 downto 0) after delay;
				elsif tmp = "01" then -- STOR(P, N)
					dataOut <= "101" & reg(1 downto 0) after delay;
				elsif tmp = "10" then -- STOR(B, R)
					dataOut <= "110" & reg(1 downto 0) after delay;
				elsif tmp = "11" then -- STOR(P, R)
					dataOut <= "111" & reg(1 downto 0) after delay;
				else
					dataOut <= (others => 'Z') after delay;
				end if;
			-- NOT USED
		else
			dataOut <= (others => 'Z') after delay;
		end if;
	end if;
end if;
end process;

end IDArchitecture;