library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CU is
	generic
	(
		DataLength : integer := 5;
		Delay : time := 1 ns
	);
	port(
		clk		: IN STD_LOGIC;	
		rst		: in std_logic;
		
		dataIn : in std_logic_vector(DataLength - 1 downto 0 );
		--dataIn(1) - source register
		--dataIn(0) - destination register
		
		MR	: out std_logic;
		MW : out std_logic;
		
		RE_ToMemory : out std_logic;
		RE_ToBus : out std_logic;
		WE_FromMemory : out std_logic;
		WE_FromBus : out std_logic;
		
		DECODE : out std_logic;
		CIR : out std_logic;
		INCR : out std_logic;
		OE_RegisterIR : out std_logic;
		
		IE_RegisterIMR: out std_logic;
		OE1_RegisterIMR: out std_logic;
		OE2_RegisterIMR: out std_logic;
		OE3_RegisterIMR: out std_logic;
		
		IE_RegisterIMRA: out std_logic;
		OE1_RegisterIMRA: out std_logic;
		OE2_RegisterIMRA: out std_logic;
				
		IE_RegisterA: out std_logic;
		IE2_RegisterA : out std_logic;
		OE1_RegisterA: out std_logic;
		OE2_RegisterA: out std_logic;
		OE3_RegisterA: out std_logic;
		OE4_RegisterA: out std_logic;
		
		IE_RegisterB: out std_logic;
		IE2_RegisterB : out std_logic;
		OE1_RegisterB: out std_logic;
		OE2_RegisterB: out std_logic;
		OE3_RegisterB: out std_logic;
		OE4_RegisterB: out std_logic;
		
		LAE : out std_logic;
		
		CALU : out std_logic; -- gdy 1 to CMP
		
		Cag_FromPC : out std_logic;
		Cag_FromIMRA : out std_logic;
		Cag_FromA : out std_logic;
		Cag_FromB : out std_logic;
		
		MUX_IsFromBus : out std_logic;
		
		OE_RegisterFLAGS: out std_logic;
		IE_RegisterFLAGS: out std_logic
		);
end CU;

architecture CUArchitecture of CU is

 type state is (
 s0,
 s1,
 s2,
 s3,
 
 s1_2,
 s2_2,
 s3_2,
 
 s1_3,
 s2_3,
 s3_3,
 
 s4,
 s4_output,
 s4_output2,
 s5,
 s6,
 s8,
 s9,
 s10,
 s11,
 s12,
 s13,
 s14,
 s14_MW,
 s15,
 s16,
 s17); 
signal present_state, next_state: state; 

constant MaxLimit :integer := 10;

signal currentInstruction : std_logic_vector(dataLength -1 downto 0);
signal counter : integer range 0 to MaxLimit - 1;
signal limit : integer range 1 to MaxLimit - 1;
signal isWorking : bit;

begin

currentInstruction <= dataIn;

stateprocess: process (clk, rst) 
begin
	if rst = '1' then
		present_state <= s0;
	elsif rising_edge(clk) then
		present_state <= next_state;
	end if;
end process; 


p2: process (present_state, currentInstruction)
  begin

	MR	<= '0' after delay;
	MW <= '0' after delay;
	
	RE_ToMemory<= '0' after delay;
	RE_ToBus <= '0' after delay;

	WE_FromMemory <= '0' after delay;
	WE_FromBus<= '0' after delay;
		
	DECODE <= '0' after delay;
	CIR <= '0' after delay;
	INCR <= '0' after delay;
	OE_RegisterIR <= '0' after delay;
	IE_RegisterIMR <= '0' after delay; -- IMR - arg natychmiastowy
	OE1_RegisterIMR <= '0' after delay; -- konwencja - OE1 jest do szyny danych DB, OE2 do AG, OE3 do szyny X, OE4 do szyny Y
	OE2_RegisterIMR <= '0' after delay;
	OE3_RegisterIMR <= '0' after delay;
	
	IE_RegisterIMRA <= '0' after delay;
	OE1_RegisterIMRA <= '0' after delay; -- offset
	OE2_RegisterIMRA <= '0' after delay;	
	IE_RegisterA <= '0' after delay;
	IE2_RegisterA <= '0' after delay;
	OE1_RegisterA <= '0' after delay; 
	OE2_RegisterA <= '0' after delay; 
	OE3_RegisterA <= '0' after delay; 
	OE4_RegisterA <= '0' after delay; 
	
	IE_RegisterB <= '0' after delay;
	IE2_RegisterB <= '0' after delay;
	OE1_RegisterB <= '0' after delay; 
	OE2_RegisterB <= '0' after delay; 
	OE3_RegisterB <= '0' after delay; 
	OE4_RegisterB <= '0' after delay; 

	LAE <= '0' after delay;
	CALU <= '0' after delay;
	
	Cag_FromPC <= '0' after delay;
	Cag_FromIMRA <= '0' after delay;
	Cag_FromA <= '0' after delay;
	Cag_FromB <= '0' after delay;
	
	MUX_IsFromBus <= '0' after delay;
		
	OE_RegisterFLAGS <= '0' after delay;
	IE_RegisterFLAGS <= '0' after delay;

case present_state is

when s0 =>
next_state <= s1;

when s1 =>
-- MAR <- (PC), (PC)++
	Cag_FromPC <= '1' after delay;
	LAE <= '1' after delay;
	INCR <= '1' after delay;

	next_state <= s2;

when s2 =>
	MR <= '1' after delay;
	WE_FromMemory <= '1' after delay;

	next_state <= s3;  
				
when s3 =>
	RE_ToBus <= '1' after delay;

	next_state <= s1_2;
	
when s1_2 =>
	CIR <= '1' after delay;
	Cag_FromPC <= '1' after delay;
	LAE <= '1' after delay;
	INCR <= '1' after delay;

	next_state <= s2_2;

when s2_2 =>
	MR <= '1' after delay;
	WE_FromMemory <= '1' after delay;

	next_state <= s3_2;  
				
when s3_2 =>
	RE_ToBus <= '1' after delay;
	IE_RegisterIMR <= '1' after delay;

	next_state <= s1_3;	
	
when s1_3 =>
	CIR <= '1' after delay;
	Cag_FromPC <= '1' after delay;
	LAE <= '1' after delay;
	INCR <= '1' after delay;

	next_state <= s2_3;

when s2_3 =>
	MR <= '1' after delay;
	WE_FromMemory <= '1' after delay;

	next_state <= s3_3;  
				
when s3_3 =>
	RE_ToBus <= '1' after delay;
	IE_RegisterIMRA <= '1' after delay;

	next_state <= s4;
       
when s4 =>
	CIR <= '1' after delay;
	
	next_state <= s4_output;

when s4_output => 
	OE_RegisterIR <= '1' after delay;

	next_state <= s4_output2; 

when s4_output2 =>
	DECODE <= '1' after delay;
	
	next_state <= s5; 
		
when s5 =>  
	if    currentInstruction(dataLength-1 downto dataLength -3) = "000" 	then next_state <= s6;
	elsif currentInstruction(dataLength-1 downto dataLength -3) = "001" 	then next_state <= s8;
	elsif currentInstruction(dataLength-1 downto dataLength -3) = "010" 	then next_state <= s9;
	elsif currentInstruction(dataLength-1 downto dataLength -3) = "011" 	then next_state <= s12;
	elsif currentInstruction(dataLength-1 downto dataLength -3) = "100" 	then next_state <= s13;
	elsif currentInstruction(dataLength-1 downto dataLength -3) = "101" 	then next_state <= s16;
	elsif currentInstruction(dataLength-1 downto dataLength -3) = "110" 	then next_state <= s15;
	elsif currentInstruction(dataLength-1 downto dataLength -3) = "111" 	then next_state <= s17;
	else next_state <= s1;
	end if;

--------------------------------------------------
--CMP(R, N)
--------------------------------------------------
when s6 =>  
	OE3_RegisterIMR <= '1' after delay;

	if(currentInstruction(0) = '0')
	then
		OE4_RegisterA <= '1' after delay;
	else
		OE4_RegisterB <= '1' after delay;
	end if;
	
	MUX_IsFromBus <= '0' after delay;
	CALU <= '1' after delay;   

	IE_RegisterFLAGS <= '1' after delay;
	next_state <= s1;  



--------------------------------------------------
-- CMP(R, R)
--------------------------------------------------
when s8 =>  
	if(currentInstruction(1) = '0')
	then
		OE3_RegisterA <= '1' after delay;
	else
		OE3_RegisterB <= '1' after delay;
	end if;
	
	if(currentInstruction(0) = '0')
	then
		OE4_RegisterA <= '1' after delay;
	else
		OE4_RegisterB <= '1' after delay;
	end if;
	
	MUX_IsFromBus <= '0' after delay;
	CALU <= '1' after delay;
	IE_RegisterFLAGS <= '1' after delay;
	next_state <= s1;    

--------------------------------------------------
-- LOAD(R, B)
--------------------------------------------------

when s9 =>
	if ( currentInstruction(1) = '0' ) 
	then
		OE2_RegisterA <= '1' after delay;
		Cag_FromA <= '1' after delay;
	else
		OE2_RegisterB <= '1' after delay;
		Cag_FromB <= '1' after delay;
	end if;
	LAE <= '1' after delay;
	next_state <= s10;

when s10 =>
	MR <= '1' after delay;
	WE_FromMemory <= '1' after delay;
	
	next_state <= s11;

when s11 =>
	RE_ToBus <= '1' after delay;
	if ( currentInstruction(0) = '0' ) 
	then
		IE_RegisterA <= '1' after delay;
	else
		IE_RegisterB <=  '1' after delay;
	end if;
	
	next_state <= s1;

--------------------------------------------------
-- LOAD(R, P)
--------------------------------------------------
when s12 =>  
	OE2_RegisterIMRA <= '1' after delay;
	Cag_FromIMRA <= '1' after delay;
	LAE <= '1' after delay;
	next_state <= s10;

--------------------------------------------------
-- STOR(B, N)
--------------------------------------------------
when s13 => 
	if ( currentInstruction(0) = '0' ) 
	then
		OE2_RegisterA <= '1' after delay;
		Cag_FromA <= '1' after delay;
	else
		OE2_RegisterB <=  '1' after delay;
		Cag_FromB <= '1' after delay;
	end if;
	
	LAE <= '1' after delay;
	OE1_RegisterIMR <= '1' after delay;
	WE_FromBus <= '1' after delay;
	
	next_state <= s14;

when s14 => 

	RE_ToMemory <= '1' after delay;

	next_state <= s14_MW;
	
when s14_MW =>
	MW <= '1' after delay;
	
	next_state <= s1;

--------------------------------------------------
-- STOR(B, R)
--------------------------------------------------
when s15 =>  
	if ( currentInstruction(1) = '0' ) 
	then
		OE1_RegisterA <= '1' after delay;
	else
		OE1_RegisterB <=  '1' after delay;
	end if;
	WE_FromBus <= '1' after delay;
	
	if ( currentInstruction(0) = '0' ) 
	then
		OE2_RegisterA <= '1' after delay;
		Cag_FromA <= '1' after delay;
	else
		OE2_RegisterB <=  '1' after delay;
		Cag_FromB <= '1' after delay;
	end if;
	
	LAE <= '1' after delay;

	next_state <= s14;

--------------------------------------------
-- STOR(P, N)
--------------------------------------------
when s16 =>
	Cag_FromIMRA <= '1' after delay;
	OE1_RegisterIMR <= '1' after delay;
	WE_FromBus <= '1' after delay;
	OE2_RegisterIMRA <= '1' after delay;
	LAE <= '1' after delay;

	next_state <= s14;

--------------------------------------------
-- STOR(P, R)
--------------------------------------------

when s17 =>
	if ( currentInstruction(1) = '0' ) 
	then
		OE1_RegisterA <= '1' after delay;
	else
		OE1_RegisterB <=  '1' after delay;
	end if;
	WE_FromBus <= '1' after delay;
			
	Cag_FromIMRA <= '1' after delay;
	OE2_RegisterIMRA <= '1' after delay;
	LAE <= '1' after delay;

	next_state <= s14;

end case;
end process;

end CUArchitecture;