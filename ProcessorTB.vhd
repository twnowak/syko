library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.SYKO.all;

entity ProcessorTB is
	generic
	(
		DataLength : integer := 5;
		InstructionLengthInBytes : integer := 3;
		Delay : time := 1 ns
	);
end ProcessorTB;

architecture ProcessorTBArchitecture of ProcessorTB is

component Processor is
	generic
	(
		DataLength : integer := 5;
		InstructionLengthInBytes : integer := 3;
		Delay : time := 1 ns
	);
	port
	(
		clk		: in STD_LOGIC;	
		rst		: in std_logic;
	

	out_dataIn : out std_logic_vector(DataLength - 1 downto 0 );
	
				out_test		: out std_logic_vector(23 downto 0 );
	out_MR : out  std_logic;
	out_MW : out  std_logic;
	out_RE_ToMemory : out  std_logic;
	out_RE_ToBus : out  std_logic;

	out_WE_FromMemory : out  std_logic;
	out_WE_FromBus : out  std_logic;

	out_DECODE : out  std_logic;
	out_CIR : out  std_logic;
	out_INCR : out  std_logic;
	out_OE_RegisterIR : out std_logic;

	out_IE_RegisterIMR: out  std_logic;
	out_OE1_RegisterIMR: out  std_logic;
	out_OE2_RegisterIMR: out  std_logic;
	out_OE3_RegisterIMR: out  std_logic;

	out_IE_RegisterIMRA: out  std_logic;
	out_OE1_RegisterIMRA: out  std_logic;
	out_OE2_RegisterIMRA: out  std_logic;

	out_IE_RegisterA: out  std_logic;
	out_IE2_RegisterA : out std_logic;
	out_OE1_RegisterA: out  std_logic;
	out_OE2_RegisterA: out  std_logic;
	out_OE3_RegisterA: out  std_logic;
	out_OE4_RegisterA: out  std_logic;

	out_IE_RegisterB: out  std_logic;
	out_IE2_RegisterB : out  std_logic;
	out_OE1_RegisterB: out  std_logic;
	out_OE2_RegisterB: out  std_logic;
	out_OE3_RegisterB: out  std_logic;
	out_OE4_RegisterB: out  std_logic;

	out_LAE : out  std_logic;
	out_CALU : out  std_logic; -- gdy 1 to CMP

	out_Cag_FromPC : out  std_logic;
	out_Cag_FromIMRA : out  std_logic;
	out_Cag_FromA : out  std_logic;
	out_Cag_FromB : out  std_logic;

	out_ALU_ToFLAGS: out std_logic_vector (2 downto 0); --sygna� z alu do rejestru flags

	out_MUX_IsFromBus : out  std_logic;
	out_MUX_Out : out std_logic_vector(7 downto 0);

	out_IE_RegisterFLAGS: out std_logic;
	out_OE_RegisterFLAGS: out  std_logic;

	out_fullInstruction : out std_logic_vector( 8*InstructionLengthInBytes - 1 downto 0);
	out_DB : out std_logic_vector( 7 downto 0);
	out_ADR : out std_logic_vector (7 downto 0);
	out_address : out std_logic_vector(7 downto 0);
	out_memoryValue : out std_logic_vector(7 downto 0);

	out_X_Bus : out std_logic_vector(7 downto 0);
	out_Y_Bus : out std_logic_vector(7 downto 0);
	out_Z_Bus : out std_logic_vector(7 downto 0);

	out_AG_FromPC : out std_logic_vector(7 downto 0);
	out_AG_FromIMRA : out std_logic_vector(7 downto 0);
	out_AG_FromA : out std_logic_vector(7 downto 0);
	out_AG_FromB : out std_logic_vector(7 downto 0);
	out_AG_FromIMR : out std_logic_vector(7 downto 0);
	
	out_memory : out ram
	);
end component Processor;


signal clk : std_logic;
signal rst : std_logic;

signal		dataIn : std_logic_vector(DataLength - 1 downto 0 );
		
signal		MR	:  std_logic;
signal		MW :  std_logic;
signal		RE_ToMemory :  std_logic;
signal		RE_ToBus :  std_logic;
		
signal		WE_FromMemory :  std_logic;
signal		WE_FromBus :  std_logic;
		
signal		DECODE :  std_logic;
signal		CIR :  std_logic;
signal		INCR :  std_logic;
signal 		OE_RegisterIR : std_logic;
		
signal		IE_RegisterIMR:  std_logic;
signal		OE1_RegisterIMR:  std_logic;
signal		OE2_RegisterIMR:  std_logic;
signal		OE3_RegisterIMR:  std_logic;
		
signal		IE_RegisterIMRA:  std_logic;
signal		OE1_RegisterIMRA:  std_logic;
signal		OE2_RegisterIMRA:  std_logic;
				
signal		IE_RegisterA:  std_logic;
signal		IE2_RegisterA : std_logic;
signal		OE1_RegisterA:  std_logic;
signal		OE2_RegisterA:  std_logic;
signal		OE3_RegisterA:  std_logic;
signal		OE4_RegisterA:  std_logic;
		
signal		IE_RegisterB:  std_logic;
signal		IE2_RegisterB :  std_logic;
signal		OE1_RegisterB:  std_logic;
signal		OE2_RegisterB:  std_logic;
signal		OE3_RegisterB:  std_logic;
signal		OE4_RegisterB:  std_logic;
		
signal		LAE :  std_logic;
signal		CALU :  std_logic; -- gdy 1 to CMP
		
signal		Cag_FromPC :  std_logic;
signal		Cag_FromIMRA :  std_logic;
signal		Cag_FromA :  std_logic;
signal		Cag_FromB :  std_logic;

signal		ALU_ToFLAGS: std_logic_vector (2 downto 0); --sygna� z alu do rejestru flags
		
signal		MUX_IsFromBus :  std_logic;
signal		MUX_Out : std_logic_vector(7 downto 0);

signal 		IE_RegisterFLAGS: std_logic;
signal		OE_RegisterFLAGS:  std_logic;

signal 		fullInstruction : std_logic_vector( 	8*	InstructionLengthInBytes - 1 downto 0);
signal 		DB : std_logic_vector( 7 downto 0);
signal		ADR : std_logic_vector (7 downto 0);
signal		address : std_logic_vector(7 downto 0);
signal		memoryValue : std_logic_vector(7 downto 0);

signal 		X_Bus : std_logic_vector(7 downto 0);
signal		Y_Bus : std_logic_vector(7 downto 0);
signal		Z_Bus : std_logic_vector(7 downto 0);

signal		AG_FromPC : std_logic_vector(7 downto 0);
signal		AG_FromIMRA : std_logic_vector(7 downto 0);
signal		AG_FromA : std_logic_vector(7 downto 0);
signal 		AG_FromB : std_logic_vector(7 downto 0);
signal		AG_FromIMR :std_logic_vector(7 downto 0);
signal 		test		: std_logic_vector(23 downto 0 );

signal memory : ram;

constant clk_period : time := 10 ns;

begin

processorunit: Processor port map (clk, rst,
dataIn,
test,
MR,
MW,
RE_ToMemory,
RE_ToBus,

WE_FromMemory,
WE_FromBus,

DECODE,
CIR,
INCR,
OE_RegisterIR,

IE_RegisterIMR,
OE1_RegisterIMR,
OE2_RegisterIMR,
OE3_RegisterIMR,

IE_RegisterIMRA,
OE1_RegisterIMRA,
OE2_RegisterIMRA,

IE_RegisterA,
IE2_RegisterA,
OE1_RegisterA,
OE2_RegisterA,
OE3_RegisterA,
OE4_RegisterA,

IE_RegisterB,
IE2_RegisterB,
OE1_RegisterB,
OE2_RegisterB,
OE3_RegisterB,
OE4_RegisterB,

LAE,
CALU,

Cag_FromPC,
Cag_FromIMRA,
Cag_FromA,
Cag_FromB,

ALU_ToFLAGS,

MUX_IsFromBus,
MUX_Out,

IE_RegisterFLAGS,
OE_RegisterFLAGS,

fullInstruction,
DB,
ADR,
address,
memoryValue,

X_Bus,
Y_Bus,
Z_Bus,

AG_FromPC,
AG_FromIMRA,
AG_FromA,
AG_FromB,
AG_FromIMR,
memory
);


clk_process :process
   begin
        clk <= '1';
        wait for clk_period/2;   
        clk <= '0';
        wait for clk_period/2;   
   end process;

  stim_proc: process
   begin       
		rst <= '1';
		wait for 1 ns;
		
		rst <= '0';

      wait;
  end process;

end ProcessorTBArchitecture;