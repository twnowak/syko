library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity IRTB is
	generic
	(
		DataLength : integer := 8;
		DELAY : time := 1 ns
	);
	port(
		clk		: IN STD_LOGIC;
		rst		: IN std_logic;

		dataIn 	: in std_logic_vector(DataLength - 1 downto 0 );
		dataOut	: out std_logic_vector(DataLength - 1 downto 0 );
		IE 		: in std_logic;
		OE 		: in std_logic
		);
end IRTB;

architecture IRTBArchitecture of IRTB is

signal tmp : std_logic_vector (DataLength - 1 downto 0);

begin

irProcess: process (CLK, IE, OE, rst)
begin
	if (rst='0') then
		tmp <= ( others => '0');
	elsif (falling_edge(CLK)) then
		if IE='1' then
			tmp <= dataIn;
		elsif (OE='1') then
			dataOut <= tmp after delay;
		else
			dataOut <= (others => 'Z') after delay;
		end if;
	end if;
end process;

end IRTBArchitecture;