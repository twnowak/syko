library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MBR is
	generic
	(
		DataLength : integer;
		Delay : time := 1 ns
	);
	port(
--buf : inout std_logic_vector( DataLength -1 downto 0);
		clk : in std_logic;
		rst : in std_logic;
		memoryData : inout std_logic_vector(DataLength - 1 downto 0 );
		busData: inout std_logic_vector(DataLength - 1 downto 0 );
		RE_ToMemory : in std_logic;
		RE_ToBus : in std_logic;
		WE_FromMemory : in std_logic;
		WE_FromBus : in std_logic
		);
end MBR;

architecture MBRArchitecture of MBR is

signal reg : std_logic_vector( DataLength -1 downto 0);

begin
--buf <= reg;
process (rst, RE_ToBus, WE_FromBus, RE_ToMemory, WE_FromMemory, clk)
begin
	if rst = '1' then
		reg <= (others => '0') after delay;
	elsif( falling_edge(clk) ) then
	
		if RE_ToBus = '1' then --zapisanie wartości na szynę danych
			busData <= reg after delay;
		elsif WE_FromBus = '1' then --zapisanie wartości z szyny danych
			reg <= busData after delay;
		elsif RE_ToMemory = '1' then --zapisanie wartości do bloku pamięci
			memoryData <= reg after delay;
		elsif WE_FromMemory = '1' then --zapisanie wartości wystawionej przez pamięć
			reg <= memoryData after delay;
		else
			busData <= (others => 'Z') after delay;
			memoryData <= (others => 'Z') after delay;
		end if;
	
	end if;
end process;

end MBRArchitecture;