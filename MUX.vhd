library IEEE; 
use IEEE.STD_LOGIC_1164.all;

entity MUX is

generic(delay : time := 1 ns);

port( 
  data_db : in std_logic_vector  (7 downto 0);
  data_out : out std_logic_vector  (7 downto 0);
  data_z  : in std_logic_vector  (7 downto 0); 
  from_bus : in std_logic --db
  );
end MUX;

architecture behav of MUX is
begin
  
process (data_db, data_z, from_bus)
begin

if from_bus = '1' then
    data_out <= data_db after delay;
else
    data_out <=data_z after delay;
end if;
  
end process;
end behav;
