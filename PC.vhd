library IEEE; 
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all; 

entity PC is

generic(delay : time := 1 ns);

port(
clk : in std_logic;
INCR : in std_logic;
reset : in std_logic;
outAG : out std_logic_vector (7 downto 0):="00000000"
);
end PC;  
 
architecture behav of PC is
  signal counter : std_logic_vector (7 downto 0);
begin

process (clk, reset)
begin
if reset = '1' then
	counter <= "00000000";
elsif rising_edge(clk) then
	if INCR = '1' then
	counter <= std_logic_vector( unsigned(counter) + "00000001" ) after delay;
	end if;
end if;
outAG <= counter;
end process;
end behav; 