library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MAR is
	generic
	(
		AddressLength : integer := 8;
		Delay : time := 1 ns
	);
	port(
		clk : IN std_logic;
		rst :		IN std_logic;

		dataIn : in std_logic_vector(AddressLength - 1 downto 0 );
		dataOut: out std_logic_vector(AddressLength - 1 downto 0 );
		LAE : in std_logic
		);
end MAR;

architecture MARArchitecture of MAR is

signal reg : std_logic_vector(AddressLength - 1 downto 0);
	
begin

	extractProcess: process (rst, dataIn, LAE, clk)
	begin

		if(rst = '1') then
			reg <= (others => 'Z') after Delay;
		elsif( falling_edge(clk) ) then 
			if LAE = '1' then
				reg <= dataIn after Delay;
			end if;
		end if;
	end process;

dataOut <= reg;

end MARArchitecture;