library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity IR is
	generic
	(
		DataLength : integer := 8;
		InstructionLengthInBytes : integer := 3;
		Delay : time := 1 ns
	);
	port(
		clk		: IN STD_LOGIC;
		rst		: IN std_logic;
		test		: out std_logic_vector(DataLength * InstructionLengthInBytes  - 1 downto 0 );
		dataIn 	: in std_logic_vector(DataLength - 1 downto 0 );
		dataOut	: out std_logic_vector(DataLength * InstructionLengthInBytes  - 1 downto 0 );
		IE 		: in std_logic;
		OE 		: in std_logic
		);
end IR;

architecture IRArchitecture of IR is

signal tmp : std_logic_vector (DataLength * InstructionLengthInBytes - 1 downto 0);
signal counter : integer range 0 to InstructionLengthInBytes - 1 ;

begin
test <= tmp;

irProcess: process (CLK, IE, OE, rst)
begin
	if (rst='1') then
		tmp <= ( others => '0') after delay;
		counter <= 0 after delay;
	elsif (falling_edge(CLK)) then
		if (IE='1') then
			tmp( DataLength * ( counter + 1) - 1 downto  DataLength * counter ) <= dataIn after delay;
			
			if ( counter = InstructionLengthInBytes - 1 ) then
				counter <= 0 after delay;
			else
				counter <= counter + 1 after delay;
			end if;
		end if;
		
		if (OE='1') then
			dataOut <= tmp after delay;
		else
			dataOut <= (others => 'Z') after delay;
		end if;
	end if;
end process;

end IRArchitecture;