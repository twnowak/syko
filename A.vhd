library ieee;
use IEEE.STD_LOGIC_1164.all;

entity A is

generic(delay : time := 1 ns);

port(
	clk: in std_logic;
 reset : in std_logic;
  data_mux : in std_logic_vector  (7 downto 0); --wejście z multipleksera
  data_x : out std_logic_vector  (7 downto 0); --wyjście na szynę lokalną X
  data_y : out std_logic_vector  (7 downto 0); --wyjście na szynę lokalną Y
  data_db  : inout std_logic_vector  (7 downto 0); --wyjście na szynę danych
  data_ag  : out std_logic_vector  (7 downto 0); -- wyjścię do generatora adresów
  IE : in std_logic; --zezwolenie na odczyt z szyny db
  IE2 : in std_logic; --zezwolenie na odczyt z multipleksera
  OE1 : in std_logic; --zezwolenie na zapis na szynę danych
  OE2 : in std_logic; --zezwolenie na zapis do generatora adresów
  OE3 : in std_logic; --zezwolenie na zapis na szynę X
  OE4 : in std_logic --zezwolenie na zapis na szynę Y
  );
end A;

architecture behav of A is
signal reg : std_logic_vector(7 downto 0) := "00000000";
begin
  
process (reset, IE, IE2, OE1, OE2, OE3, OE4, clk)
begin
  
if reset = '1' then
reg <= "00000000";

elsif IE = '1' then --Odczyt wartości z db
reg <= data_db after delay;
    
elsif IE2 = '1' then --Odczyt wartosci z mux 
reg <= data_mux after delay;
    
elsif OE1 = '1' then --Wystawienie wartosci na szyne db 
data_db <= reg after delay;
    
elsif OE2 = '1' then --Wystawienie wartosci do AG
data_ag <= reg after delay; 
    
elsif OE3 = '1' then --Wystawienie wartosci na szyne x
data_x <= reg after delay;

elsif OE4= '1' then --Wystawienie wartosci na szyne y
data_y <= reg after delay;
    
else
data_x <= "ZZZZZZZZ" after delay;
data_y <= "ZZZZZZZZ" after delay;
data_db <= "ZZZZZZZZ" after delay;
data_ag <= "ZZZZZZZZ" after delay;
end if;
  
end process;
end behav;
