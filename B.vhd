library IEEE; 
use IEEE.STD_LOGIC_1164.all;

entity B is

generic(delay : time := 1 ns);

port( reset : in std_logic;
  data_mux : in std_logic_vector  (7 downto 0);
  data_x : out std_logic_vector  (7 downto 0);
  data_y : out std_logic_vector  (7 downto 0);
  data_db  : inout std_logic_vector  (7 downto 0); 
  data_ag  : out std_logic_vector  (7 downto 0); 
  IE : in std_logic; --szyna db
  IE2 : in std_logic; --mux
  OE1 : in std_logic; --szynadb
  OE2 : in std_logic; --ag
  OE3 : in std_logic; --x
  OE4 : in std_logic --y
  );
end B;

architecture behav of B is
signal reg : std_logic_vector(7 downto 0) := "00000000";
begin
  
process (reset, IE, IE2, OE1, OE2, OE3, OE4)
begin
  
if reset = '1' then
reg <= "00000000";

elsif IE = '1' then --Odczyt wartości z db
reg <= data_db after delay;
    
elsif IE2 = '1' then --Odczyt wartosci z mux 
reg <= data_mux after delay;
    
elsif OE1 = '1' then --Wystawienie wartosci na szyne db 
data_db <= reg after delay;
    
elsif OE2 = '1' then --Wystawienie wartosci do AG
data_ag <= reg after delay; 
    
elsif OE3 = '1' then --Wystawienie wartosci na szyne x
data_x <= reg after delay;

elsif OE4= '1' then --Wystawienie wartosci na szyne y
data_y <= reg after delay;
    
else
data_x <= "ZZZZZZZZ" after delay;
data_y <= "ZZZZZZZZ" after 5 us;
data_db <= "ZZZZZZZZ" after 5 us;
data_ag <= "ZZZZZZZZ" after 20 us;
end if;
  
end process;
end behav;
