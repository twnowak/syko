library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MARTB is
	generic
	(
		PayloadLengthInBits : integer;
		FrameLengthInBits : integer
	);
	port(
		clk		: IN STD_LOGIC;	

		dataIn : in std_logic_vector(FrameLengthInBits - 1 downto 0 );
		dataOut: out std_logic_vector(PayloadLengthInBits - 1 downto 0 );
		writeEnabled : in std_logic;
		dataReady : out std_logic
		);
end MARTB;

architecture MARTBArchitecture of MARTB is

begin

extractProcess: process (clk, dataIn, writeEnabled)
begin
if clk = '1' and clk'event
then
	if ( writeEnabled = '1' )
	then
		dataOut(PayloadLengthInBits - 1 downto 0 ) <= dataIn ( FrameLengthInBits - 3 downto FrameLengthInBits - PayloadLengthInBits - 2 );
		dataReady <= '1';
	else
		dataReady <= '0';
	end if;
end if;
end process;

end MARTBArchitecture;