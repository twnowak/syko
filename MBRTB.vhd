library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MBRTB is
	generic
	(
		DataLength : integer := 5;
		InstructionLengthInBytes : integer := 3;
		Delay : time := 1 ns
	);
end MBRTB;

architecture MBRTBArchitecture of MBRTB is

component MBR is
	generic
	(
		DataLength : integer := 8;
		Delay : time := Delay
	);
	port(
buf : inout std_logic_vector( DataLength -1 downto 0);
		rst : in std_logic;
		memoryData : inout std_logic_vector(DataLength - 1 downto 0 );
		busData: inout std_logic_vector(DataLength - 1 downto 0 );
		RE_ToMemory : in std_logic;
		RE_ToBus : in std_logic;
		WE_FromMemory : in std_logic;
		WE_FromBus : in std_logic
		);
end component MBR;


signal clk : std_logic;
signal rst : std_logic;
signal		RE_ToMemory :  std_logic;
signal		RE_ToBus :  std_logic;	
signal		WE_FromMemory :  std_logic;
signal		WE_FromBus :  std_logic;
signal 		DB : std_logic_vector( 7 downto 0);
signal		memoryValue : std_logic_vector(7 downto 0);
signal  buf : std_logic_vector( 7 downto 0);
constant clk_period : time := 5 ns;

begin

mbrunit: MBR port map ( buf, rst, memoryValue, DB, RE_ToMemory, RE_ToBus, WE_FromMemory, WE_FromBus);

clk_process :process
   begin
        clk <= '1';
        wait for clk_period/2;   
        clk <= '0';
        wait for clk_period/2;   
   end process;

  stim_proc: process
   begin       
		rst <= '1';
		wait for 1 ns;
		
		rst <= '0';
		wait for 4 ns;
		WE_FromMemory <= '1';
		wait for 1 ns;
		memoryValue <= "00110011";
		wait for 4 ns; 
		WE_FromMemory <= '0';
		RE_ToBus <= '1';
		wait for 1 ns; 
		memoryValue <= "ZZZZZZZZ";
		wait for 4 ns; 
		RE_ToBus <= '0';
		
      wait;
  end process;

end MBRTBArchitecture;